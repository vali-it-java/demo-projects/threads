package com.valiit;

public class App {

    public static void main(String[] args) {
        Object lock = new Object();

        Thread myThreadObj1 = new MyThread("1", lock);
        Thread myThreadObj2 = new MyThread("2", lock);
        Thread myThreadObj3 = new MyThread("3", lock);
        Thread myThreadObj4 = new MyThread("4", lock);
        Runnable myRunnableObj1 = new MyRunnable("R1");
        Thread myThreadObj5 = new Thread(myRunnableObj1);

        myThreadObj1.start();
        myThreadObj2.start();
        myThreadObj3.start();
        myThreadObj4.start();
        myThreadObj5.start();

        System.out.println("Main meetod lõpetab tegevuse nüüd!");
    }

    static class MyRunnable implements Runnable {

        private String id = null;

        public MyRunnable(String id) {
            this.id = id;
        }

        public void run() {
            this.easySleep();
            System.out.println("Runnable runs: " + this.id);
        }

        private void easySleep() {
            try {
                long seconds = (long) (Math.random() * 10 * 1000);
                System.out.println(String.format("Runnable %s: I am going to sleep %s seconds", this.id, seconds / 1000.0));
                Thread.sleep(seconds);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    static class MyThread extends Thread {

        private String id = null;
        private Object lock = null;

        public MyThread(String id, Object lock) {
            this.id = id;
            this.lock = lock;
        }

        @Override
        public void run() {
            easySleep(this.id, lock);
            System.out.println("I am going to run now: " + this.id);
        }

//        private synchronized void easySleep(String id) {
        private static synchronized void easySleep(String id, Object lock) {
            synchronized (lock) {
                try {
                    long seconds = (long) (Math.random() * 10 * 1000);
                    System.out.println(String.format("Thread %s: I am going to sleep for %s seconds", id, seconds / 1000.0));
                    Thread.sleep(seconds);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
